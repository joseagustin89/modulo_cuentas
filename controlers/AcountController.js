

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edjada/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;





  function getAccountsV2 (req, res) {
    console.log("GET /apitechu/v2/Accounts");

    var httpClient = requestJson.createClient(mlabBaseURL);
    console.log("Client Created");

    httpClient.get("accounts?" + mlabAPIKey,
      function(err,resMlab,body){
        var response = !err ?
        //aqui prodirmamos gestionar los codigos de error diferentes dentro del objeto resMlab que contiene toda la informacion de la respuesta no solo el body
          body : {"msg" : "Error obtenido cuentas"};
          res.send(response);
      }
    )

}

  function getAccountIdV2 (req, res) {
    console.log("GET /apitechu/v2/getAccountbyV2");

    var httpClient = requestJson.createClient(mlabBaseURL);
    console.log("Client Created");
    var id = req.params.id;
    console.log(id);
    var query = 'q={"userId": '+ id +'}';
    console.log("accounts?" + query + "&" + mlabAPIKey);


    httpClient.get("accounts?" + query + "&" + mlabAPIKey,
      function(err,resMlab,body){

        if(err){
          var response = {
              "msg" : "Error obteniendo la cuenta"
          };
          res.status(500);
        } else {
          if (body.length > 0){
            var response = body;
          }else {
            var response = {
              "msg" : "cuenta no encontrada"
            };
            res.status(404);
          }
        }
        res.send(response);
      }
    )

}

function createUserV2(req, res) {
    console.log("POST /apitechu/v2/users");
    console.log(req.body.id);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.password);

    var newUser = {
      "id" :req.body.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : crypt.hash(req.body.password)
    };

    var httpClient = requestJson.createClient(mlabBaseURL);
    console.log("Client Created");

    httpClient.post("user?" + mlabAPIKey, newUser,
      function(err,resMlab,body){
        console.log("usuario guardado con exito");
        res.status(201);
        res.send({"msg" : "Usuario creado con exito"});
      }
    )

  }

  module.exports.getAccountsV2 = getAccountsV2;
  module.exports.getAccountIdV2 = getAccountIdV2;
