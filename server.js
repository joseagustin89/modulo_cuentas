const express = require('express');

require('dotenv').config();

const app = express();

const io = require('./io');

const userController = require('./controlers/UserController');

const authController = require('./controlers/AuthController');

const accountController = require('./controlers/AcountController');

//para que la aplicacion en el body parsee Json es la siguiente linea
app.use(express.json());

//para permitir rutas de distintos dominios

var enableCORS = function(req, res, next) {
 // No producción!!!11!!!11one!!1!
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}
app.use(enableCORS);

//para que la aplicacion escuche en el puerto 3000
app.listen(3000);

app.get("/apitechu/v1/users", userController.getUsersV1);

app.post("/apitechu/v1/users", userController.createUserV1);

app.post("/apitechu/v2/users", userController.createUserV2);

app.get("/apitechu/v2/users", userController.getUsersV2);

app.get("/apitechu/v2/accounts", accountController.getAccountsV2);

app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);

app.get("/apitechu/v2/accounts/:id", accountController.getAccountIdV2);


app.get("/apitechu/v1/hello",
function (req, res) {
  console.log("hello world");
  res.send({"msg" : "hello world"});
}
);

app.delete("/apitechu/v1/users/:id", userController.deleteUsersV1);

app.post("/apitechu/v1/monstruo/:p1/:p2",
function (req, res) {
  console.log("Post /apitechu/v1/monstruo/:p1/:p2");

  console.log("Parametros");
  console.log(req.params);

  console.log("Query string");
  console.log(req.query);

  console.log("Headers");
  console.log(req.headers);

  console.log("Body");
  console.log(req.body);

  res.send("ok");
}
);

app.post("/apitechu/v1/login", authController.loginV1);
app.post("/apitechu/v2/login", authController.loginV2);

app.post("/apitechu/v1/logout", authController.logoutV1);
app.get("/apitechu/v2/logout/:id", authController.logoutV2);
